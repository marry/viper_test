//
//  UIViewController+Routes.m
//  marry
//
//  Created by marry on 2017/7/4.
//  Copyright © 2017年 marry. All rights reserved.
//

#import "UIViewController+Routes.h"

@implementation UIViewController (Routes)

- (UIViewController *)topViewController {
    return [UIViewController _sp_visibleViewControllerFrom:self];
}

+ (UIViewController *)topViewController {
    return [self _sp_visibleViewControllerFrom:[self sp_getKeyWindow].rootViewController];
}

+ (UIWindow *)sp_getKeyWindow {
    NSArray *windows = [UIApplication sharedApplication].windows;
#ifdef __IPHONE_13_0
    if (@available(iOS 13.0, *)) {
        for (UIScene *scene in UIApplication.sharedApplication.connectedScenes) {
            if ([scene isKindOfClass:UIWindowScene.class] && [((UIWindowScene *)scene).screen isEqual:UIScreen.mainScreen]) {
                windows = ((UIWindowScene *)scene).windows;
                break;
            }
        }
    }
#endif
    
    UIWindow *targetWindow = nil;
    for (UIWindow *window in windows) {
        if (!window.hidden && window.rootViewController != nil) {
            if (targetWindow == nil || window.isKeyWindow) {
                targetWindow = window;
            }
        }
    }
    return targetWindow;
}

+ (UINavigationController *)sp_topNavigationController {
    
    if (![self topViewController].navigationController) {
        UIViewController *rootViewController = [UIViewController _sp_visibleViewControllerFrom:[self sp_getKeyWindow].rootViewController];
        if ([rootViewController.class isKindOfClass:[UITableViewController class]]) {
            
            return [((UITabBarController *) rootViewController) selectedViewController].navigationController;
        }
        
        return [self sp_getKeyWindow].rootViewController.navigationController;
    }
    return [self topViewController].navigationController;
}


+ (UIViewController *)_sp_visibleViewControllerFrom:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _sp_visibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    }
    
    if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _sp_visibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    }
    
    if (vc.presentedViewController) {
        return [self _sp_visibleViewControllerFrom:vc.presentedViewController];
    }
    
    return vc;
}

+ (void)dismissPresentedViewController {
    UIViewController* viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if (viewController.presentedViewController) {
        [viewController.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }
}

+ (UIViewController *)_not_presentintViewControllerFrom:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _sp_visibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    }
    
    if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _sp_visibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    }
    
    return vc;
}

@end
