//
//  FristTableViewCell.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <UIKit/UIKit.h>
#import "FristModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FristTableViewCell : UITableViewCell

@property (nonatomic,strong) FristModel *model;

@end

NS_ASSUME_NONNULL_END
