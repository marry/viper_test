//
//  FristViewController.m
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import "FristViewController.h"
#import "FristTableViewCell.h"
#import "FristModel.h"

static NSString * TABLEVIEWCELL_IDENTIFIER = @"FristTableViewCell";

@interface FristViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@end

@implementation FristViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"frist";
    self.view.backgroundColor = [UIColor yellowColor];
    
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.rowHeight = 50;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.showsVerticalScrollIndicator = NO;
        tableView.showsVerticalScrollIndicator = NO;
        [tableView registerClass:[FristTableViewCell class]
     forCellReuseIdentifier:TABLEVIEWCELL_IDENTIFIER];
        
        [self.view addSubview:tableView];
        tableView;
    });
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([self.eventHandler respondsToSelector:@selector(event_viewWillAppear:)]) {
        [self.eventHandler event_viewWillAppear:animated];
    }
}


#pragma mark - FristViewDataSource
-(void)reloadData{
    [self.tableView reloadData];
}



#pragma mark - UITableViewDelegate | UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([self.eventHandler respondsToSelector:@selector(numberRow)]) {
        return [self.eventHandler numberRow];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FristTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TABLEVIEWCELL_IDENTIFIER forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[FristTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TABLEVIEWCELL_IDENTIFIER];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([self.eventHandler respondsToSelector:@selector(itemForIndexPath:)]) {
        FristModel *item = [self.eventHandler itemForIndexPath:indexPath];
        cell.model = item;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [UIView animateWithDuration:0.3 animations:^{
        // 稍微增加cell视图的大小
        cell.transform = CGAffineTransformMakeScale(2.0, 2.0);
    } completion:^(BOOL finished) {
        cell.transform = CGAffineTransformMakeScale(1.0, 1.0);
        
        if ([self.eventHandler respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
            [self.eventHandler didSelectRowAtIndexPath:indexPath];
        }
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
