//
//  FristTableViewCell.m
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import "FristTableViewCell.h"

@interface FristTableViewCell ()

@property (nonatomic,strong) UILabel *nameLab;
@property (nonatomic,strong) UILabel *descLab;

@end

@implementation FristTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.nameLab = ({
            UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 140, 30)];
            lab;
        });

        self.descLab = ({
            UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(185, 15, 140, 30)];
            lab;
        });
        [self.contentView addSubview:self.nameLab];
        [self.contentView addSubview:self.descLab];
    }
    return self;
}

- (void)setModel:(FristModel *)model{
    if (model) {
        self.nameLab.text = model.name;
        self.descLab.text = model.desc;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
