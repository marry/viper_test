//
//  FristViewController.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <UIKit/UIKit.h>
#import "FristViewDataSource.h"
#import "FristEventHandlerProtocol.h"
#import "EventHandleCycleProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface FristViewController : UIViewController <FristViewDataSource>

@property (nonatomic,strong) id<FristEventHandlerProtocol,EventHandleCycleProtocol> eventHandler;

@end

NS_ASSUME_NONNULL_END
