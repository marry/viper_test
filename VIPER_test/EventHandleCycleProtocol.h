//
//  EventHandleCycleProtocol.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol EventHandleCycleProtocol <NSObject>

- (void)event_viewWillAppear:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
