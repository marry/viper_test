//
//  FristEventHandlerProtocol.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <Foundation/Foundation.h>
#import "FristModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FristEventHandlerProtocol <NSObject>

- (NSUInteger)numberRow;
- (FristModel *)itemForIndexPath:(NSIndexPath *)indexPath;
- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
