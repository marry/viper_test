//
//  FristPresenter.m
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import "FristPresenter.h"

@implementation FristPresenter


#pragma mark - EventHandleCycleProtocol

- (void)event_viewWillAppear:(BOOL)animated{
    if ([self.interactor respondsToSelector:@selector(fetchData:)]) {
        [self.interactor fetchData:^(NSError * _Nonnull error) {
            if (!error) {
                __weak typeof(self) weakSelf = self;
                [weakSelf.view reloadData];
            }
        }];
    }
}


#pragma mark - FristEventHandlerProtocol

- (NSUInteger)numberRow{
    return self.interactor.dataArray.count;
}

- (FristModel *)itemForIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row >= self.interactor.dataArray.count) {
        return nil;
    }
    
    FristModel *item = self.interactor.dataArray[indexPath.row];
    return item;
}

-(void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row >= self.interactor.dataArray.count) {
        return;
    }
    
    NSLog(@"didSelectRowAtIndexPath:%ld",(long)indexPath.row);
    
    if ([self.wireframe respondsToSelector:@selector(goNewVCWithIndex:)]) {
        [self.wireframe goNewVCWithIndex:indexPath.row];
    }
}



@end
