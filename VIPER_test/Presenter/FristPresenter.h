//
//  FristPresenter.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <UIKit/UIKit.h>
#import "FristEventHandlerProtocol.h"
#import "FristWireframeProtocol.h"
#import "FristViewDataSource.h"
#import "FristInteractorProtocol.h"
#import "EventHandleCycleProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface FristPresenter : NSObject <FristEventHandlerProtocol,EventHandleCycleProtocol>

@property (nonatomic, strong) id<FristWireframeProtocol> wireframe;
@property (nonatomic, weak)   UIViewController<FristViewDataSource> *view;
@property (nonatomic, strong) id<FristInteractorProtocol> interactor;

@end

NS_ASSUME_NONNULL_END
