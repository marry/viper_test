//
//  FristInteractor.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <Foundation/Foundation.h>
#import "FristInteractorProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface FristInteractor : NSObject <FristInteractorProtocol>


@end

NS_ASSUME_NONNULL_END
