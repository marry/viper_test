//
//  FristInteractor.m
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import "FristInteractor.h"
#import "FristModel.h"

@implementation FristInteractor

@synthesize dataArray;

- (instancetype)initWithData:(NSArray<FristModel *> *)data{
    if (self = [super init]) {
        if (data) {
            self.dataArray = data;
        } else {
            self.dataArray = [NSArray array];
        }
    }
    
    return self;
}

- (void)fetchData:(void (^)(NSError * _Nonnull))completion{
    NSMutableArray *tmp = [NSMutableArray arrayWithArray:self.dataArray];
    
    //假数据模拟
    for (int i = 0; i < 50; i++) {
        FristModel *model = [FristModel new];
        int num = arc4random();
        model.name = [NSString stringWithFormat:@"名字:%d",num];
        model.desc = [NSString stringWithFormat:@"编号:%d",num];
        [tmp addObject:model];
    }
    
    self.dataArray = [tmp copy];
    
    if (completion) {
        completion(nil);
    }
}


@end
