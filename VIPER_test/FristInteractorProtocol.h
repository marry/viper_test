//
//  FristInteractorProtocol.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <Foundation/Foundation.h>
#import "FristModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FristInteractorProtocol <NSObject>

@property (nonatomic,strong) NSArray<FristModel *>* dataArray;

- (instancetype)initWithData:(NSArray<FristModel *> *)data;

- (void)fetchData:(void (^)(NSError * error))completion;

@end

NS_ASSUME_NONNULL_END
