//
//  FristModel.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FristModel : NSObject

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *desc;

@end

NS_ASSUME_NONNULL_END
