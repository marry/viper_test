//
//  FristWireframeProtocol.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FristWireframeProtocol <NSObject>

- (void)goNewVCWithIndex:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
