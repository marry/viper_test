//
//  ViewController.m
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import "ViewController.h"
#import "FristWireframe.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"viewDidLoad";
    [self.view addSubview:({
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 200, 200)];
        [btn setTitle:@"跳转" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        btn;
    })];
}

- (void)click{
    [FristWireframe pushFristVC];
}


@end
