//
//  FristViewDataSource.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <Foundation/Foundation.h>

//@class FristModel;

NS_ASSUME_NONNULL_BEGIN

@protocol FristViewDataSource <NSObject>

- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
