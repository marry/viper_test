//
//  FristWireframe.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <UIKit/UIKit.h>
#import "FristWireframeProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface FristWireframe : NSObject <FristWireframeProtocol>

+ (void)pushFristVC;

@end

NS_ASSUME_NONNULL_END
