//
//  FristWireframe.m
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import "FristWireframe.h"
#import "FristViewController.h"
#import "UIViewController+Routes.h"
#import "FristPresenter.h"
#import "FristInteractor.h"

@interface FristWireframe ()

@property (nonatomic,weak) UIViewController<FristViewDataSource> *view;

@end

@implementation FristWireframe 

+ (void)pushFristVC{
    UIViewController *fristVC = [self buildView];
    [[UIViewController sp_topNavigationController] pushViewController:fristVC animated:YES];
}


+ (UIViewController *)buildView{
    FristViewController *v = [[FristViewController alloc]init];
    FristWireframe *wireframe = [[FristWireframe alloc]init];
    FristPresenter *presenter = [[FristPresenter alloc]init];
    FristInteractor *interactor = [[FristInteractor alloc]initWithData:nil];
    
    //View 和 Presenter关联
    v.eventHandler = presenter;
    
    //Presenter 和 View、Wireframe、Interactor关联
    presenter.view = v;
    presenter.wireframe = wireframe;
    presenter.interactor = interactor;
    
    //Wireframe 和 View 关联
    wireframe.view = v;
    
    return v;
}


#pragma mark - FristWireframeProtocol

- (void)goNewVCWithIndex:(NSUInteger)index{
    NSLog(@"goNewVCWithIndex");
    UIViewController *vc = [[UIViewController alloc]init];
    vc.title = [NSString stringWithFormat:@"new vc title:%ld",index];
    vc.view.backgroundColor = [UIColor orangeColor];
    [self.view.navigationController pushViewController:vc animated:YES];
}


@end
