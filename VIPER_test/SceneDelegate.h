//
//  SceneDelegate.h
//  VIPER_test
//
//  Created by marry on 2022/4/18.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

