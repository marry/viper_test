//
//  UIViewController+Routes.h
//  marry
//
//  Created by marry on 2017/7/4.
//  Copyright © 2017年 marry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Routes)

- (UIViewController *)topViewController;
+ (UIViewController *)topViewController;
+ (UINavigationController *)sp_topNavigationController;

+ (void)dismissPresentedViewController;

+ (UIViewController *)_sp_visibleViewControllerFrom:(UIViewController *)vc;

@end
